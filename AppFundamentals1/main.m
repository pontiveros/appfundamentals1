//
//  main.m
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/17/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TAppDelegate class]));
    }
}
