//
//  TMultiThreadViewController.m
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/17/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import "TMultiThreadViewController.h"
#import "TSyncThreadViewController.h"

@interface TMultiThreadViewController ()

@end

@implementation TMultiThreadViewController

@synthesize timer1;
@synthesize lbTimer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Threading tests";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTouchTimer:(id)sender
{
    UISwitch *switchCtrl = (UISwitch*)sender;
    
    if (switchCtrl.isOn) {
        timer1 = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(runTimer) userInfo:nil repeats:YES];
        NSLog(@"NSTimer enabled.");
    } else {
        [timer1 invalidate];
        timer1 = nil;
        NSLog(@"NSTimer disabled.");
    }
}

- (IBAction)onTouchWorker1:(id)sender
{
    // Detach thread, releases resources automatically.
    [NSThread detachNewThreadSelector:@selector(worker1) toTarget:self withObject:nil];
}

- (IBAction)onTouchSync:(id)sender
{
    TSyncThreadViewController *syncThreadView = [[[TSyncThreadViewController alloc] init] autorelease];
    [self.navigationController pushViewController:syncThreadView animated:YES];
}

- (void)runTimer
{
    static int ticks = 0;
    ticks++;
    NSString *stringTimer = [NSString stringWithFormat:@"Timer running, tick number %d.", ticks];
    NSLog(@"Tick number %d", ticks);
    [self performSelectorOnMainThread:@selector(updateTimerLabel:) withObject:stringTimer waitUntilDone:YES];
}

- (void)updateTimerLabel:(NSString*)value
{
    [self.lbTimer setText:value];
}

- (void)worker1
{
    NSLog(@"Begin thread.");
    for (int i = 0; i < 10; i++) {
        NSString * stringThread = [NSString stringWithFormat:@"Thread work order %d.", (i + 1)];
        
        dispatch_async(dispatch_get_main_queue(), ^{
                [self.lbThread setText:stringThread];
            }
        );
        
        NSLog(@"Thread work order %d.", (i + 1));
        sleep(2);
    }
    NSLog(@"End of thread.");
}

- (void)worker2Synchronized
{
    @synchronized(self) {
        NSLog(@"");
    }
}

@end
