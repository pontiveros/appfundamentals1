//
//  TEditViewController.m
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/23/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import "TEditViewController.h"
#import "Customer.h"
#import "TLocation.h"
#import "TAppDelegate.h"



@interface TEditViewController ()

@end

@implementation TEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        /*
        [locations addObject:@"Vicente Lopez"];
        [locations addObject:@"Martinez"];
        [locations addObject:@"Quilmes"];
        [locations addObject:@"La Plata"];
        [locations addObject:@"San Martin"];
        [locations addObject:@"Olivos"];
        [locations addObject:@"San Justo"];
        [locations addObject:@"CABA"];
        [locations addObject:@"Pilar"];
        [locations addObject:@"Ramos Mejia"];
        [locations addObject:@"Berazategui"];
        [locations addObject:@"Hudson"];
        [locations addObject:@"Tigre"];
        [locations addObject:@"Adrogue"];
        [locations addObject:@"Berisso"];
        [locations addObject:@"Don Torcuato"];
        [locations addObject:@"Pacheco"];
        [locations addObject:@"San Isidro"];
        */
    }
    return self;
}

- (void)dealloc
{
    [locations release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Edit view";
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                            target:self
                                                                                            action:@selector(save)] autorelease];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureTap:)];
    [self.view addGestureRecognizer:tapGesture];
    
    locations = [[NSMutableArray alloc] init];
    
    TAppDelegate *appDelegate = (TAppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSEntityDescription *entDesc  = [NSEntityDescription entityForName:@"TLocation" inManagedObjectContext:appDelegate.managedObjectContext];
    NSFetchRequest      *fetchReq = [[NSFetchRequest alloc] init];
    NSError *error = nil;
    
    NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
    
    [fetchReq setEntity:entDesc];
    [fetchReq setSortDescriptors:[NSArray arrayWithObject:sortDesc]];
    
    NSArray *arr = (NSArray*)[appDelegate.managedObjectContext executeFetchRequest:fetchReq error:&error];
    
    if (!error) {
        for (TLocation *loc in arr)
            [locations addObject:loc];
    }
}

- (void)handleGestureTap:(UIGestureRecognizer*)gesture
{
    [edCode resignFirstResponder];
    [edFullname resignFirstResponder];
    [edPhone resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)save
{
    @try {
        TAppDelegate *application = [UIApplication sharedApplication].delegate;
        
        Customer *customer = [NSEntityDescription insertNewObjectForEntityForName:@"Customer" inManagedObjectContext:application.managedObjectContext];
        NSError     *error = nil;
        
        customer.code     = [NSNumber numberWithInt:[edCode.text intValue]];
        customer.fullname = edFullname.text;
        customer.phone    = edPhone.text;
        customer.county   = locationSelected;
        
        if (![application.managedObjectContext save:&error]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Error trying to save customer."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Accept"
                                                  otherButtonTitles:nil];
            
            [alert show];
            [alert release];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    } @catch (NSException *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Error trying to save customer."
                                                       delegate:nil
                                              cancelButtonTitle:@"Accept"
                                              otherButtonTitles:nil];
        
        [alert show];
        [alert release];
    }
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    TLocation   *loc = (TLocation*)[locations objectAtIndex:row];
    locationSelected = loc.fullname;
    NSLog(@"Value selected: %@", locationSelected);
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    TLocation *loc = (TLocation*)[locations objectAtIndex:row];
    return loc.fullname;
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [locations count];
}
    
@end
