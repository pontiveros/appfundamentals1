//
//  Customer.h
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/23/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Customer : NSManagedObject

@property (nonatomic, retain) NSNumber * code;
@property (nonatomic, retain) NSString * fullname;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * county;

@end
