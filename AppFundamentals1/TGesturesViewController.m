//
//  TGesturesViewController.m
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/22/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import "TGesturesViewController.h"

@interface TGesturesViewController ()

@end

@implementation TGesturesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureDown:)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDown];
    
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureUp:)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUp];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureTap:)];
    [self.view addGestureRecognizer:tapGesture];
    
    self.title = @"Gestures";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIGestureRecognizer
- (void)handleGestureDown:(UIGestureRecognizer*)gestureRecognizer
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Gesture"];
    [alert setMessage:@"down"];
    [alert addButtonWithTitle:@"Accept"];
    [alert show];
    [alert release];
}

- (void)handleGestureUp:(UIGestureRecognizer*)gestureRecognizer
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Gesture"];
    [alert setMessage:@"up"];
    [alert addButtonWithTitle:@"Accept"];
    [alert show];
    [alert release];
}

- (void)handleGestureTap:(UIGestureRecognizer*)gestureRecognizer
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Gesture"];
    [alert setMessage:@"Tap"];
    [alert addButtonWithTitle:@"Accept"];
    [alert show];
    [alert release];
}
@end
