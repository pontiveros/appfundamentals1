//
//  TListViewController.m
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/17/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import "TListViewController.h"
#import "TEditViewController.h"
#import "Customer.h"
#import "TAppDelegate.h"


@interface TListViewController ()

@end

@implementation TListViewController

@synthesize items;
@synthesize _tableView;
@synthesize lbCode;
@synthesize lbName;
@synthesize lbPhone;
@synthesize lbLocation;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadDataFromDatabase];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"List view";
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                            target:self
                                                                                            action:@selector(addItem)] autorelease];
}

- (void)loadDataFromDatabase
{
    // Begin load data from local DB.
    [self.items release];
    self.items = nil;
    TAppDelegate       *application = [UIApplication sharedApplication].delegate;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Customer" inManagedObjectContext:application.managedObjectContext];
    NSFetchRequest      *request    = [[[NSFetchRequest alloc] init] autorelease];
    
    [request setFetchBatchSize:20]; // Size of batch.
    [request setEntity:entityDesc]; // Link request to entity.
    
    [request setEntity:entityDesc];
    // [request setPredicate:[NSPredicate predicateWithFormat:@"code > 5"]]; // Condition.
    NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES]; // Sorting.
    
    [request setSortDescriptors:[NSArray arrayWithObject:sortDesc]];
    
    NSError *error = nil;
    NSArray   *arr = [(NSArray*)[application.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    [self setArray:arr];
    
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                        message:@"There were an error trying to load data."
                                                       delegate:nil
                                              cancelButtonTitle:@"Accept"
                                              otherButtonTitles:nil];
        
        [alert show];
        [alert release];
    }
    // End load data from local DB.
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{ [self._tableView reloadData]; });
}

- (void)setArray:(NSArray*)array
{
    self.items = array;
    [self._tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addItem
{
    TEditViewController *editView = [[[TEditViewController alloc] init] autorelease];
    [self.navigationController pushViewController:editView animated:YES];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Customer *customer = (Customer*)[self.items objectAtIndex:indexPath.row];
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [self.lbCode setText:[NSString stringWithFormat:@"%@", customer.code]];
                       [self.lbName setText:customer.fullname];
                       [self.lbPhone setText:customer.phone];
                       [self.lbLocation setText:customer.county];
                   }
    );
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    Customer    *customer = [self.items objectAtIndex:indexPath.row];
    [cell.textLabel setText:customer.fullname];
    return cell;
}

@end
