//
//  TSyncThreadViewController.m
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/18/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import "TSyncThreadViewController.h"

@interface TSyncThreadViewController ()

@end

@implementation TSyncThreadViewController

@synthesize worker1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Sync Thread";
    [edResult setEnabled:NO];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTouchOutEdit:(id)sender
{
}

- (void)handleTap:(UIGestureRecognizer*)gestureRecognize
{
    [edValueA resignFirstResponder];
    [edValueB resignFirstResponder];
    
    @synchronized(self) {
        @try  {
            numberA = [edValueA.text floatValue];
            numberB = [edValueB.text floatValue];
            
            [edResult setText:@"0.0"];
            result  = 0.0;
        } @catch (NSException *err) {
            numberA = 0.0;
            numberB = 0.0;
            result  = 0.0;
        }
    }
}

- (IBAction)onTouchStartWorker:(id)sender
{
    self.worker1 = [[NSThread alloc] initWithTarget:self selector:@selector(worker1:) object:nil];
    [self.worker1 start];
}

- (IBAction)onTouchStopWorker:(id)sender
{
    if (self.worker1 && self.worker1.isExecuting) {
        [self.worker1 cancel];
        [self.worker1 release];
        self.worker1 = nil;
    }
}

- (void)worker1:(id)param
{
    // static int ticks = 0;
    NSInteger ticks = 0;
    while (![[NSThread currentThread] isCancelled]) {
        ticks++;
        @synchronized (self) {
            float temp = numberA + numberB;
            result = temp + ticks;
            [self performSelectorOnMainThread:@selector(updateEditResult:) withObject:[NSNumber numberWithInt:ticks] waitUntilDone:NO];
        }
        sleep(1);
    }
}

- (void)updateEditResult:(NSNumber*)param
{
    NSInteger ticks = [param integerValue];
    [edResult setText:[NSString stringWithFormat:@"%.5f", result]];
    [progress setText:[NSString stringWithFormat:@"Worker 1 running, sequence %d", ticks]];
}

@end
