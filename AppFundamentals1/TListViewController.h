//
//  TListViewController.h
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/17/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, retain)NSArray *items;
@property(nonatomic, retain)IBOutlet UITableView *_tableView;
@property(nonatomic, retain)IBOutlet UILabel *lbCode;
@property(nonatomic, retain)IBOutlet UILabel *lbName;
@property(nonatomic, retain)IBOutlet UILabel *lbPhone;
@property(nonatomic, retain)IBOutlet UILabel *lbLocation;

- (void)setArray:(NSArray*)array;

@end
