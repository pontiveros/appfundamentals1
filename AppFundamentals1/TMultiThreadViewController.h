//
//  TMultiThreadViewController.h
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/17/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMultiThreadViewController : UIViewController

@property (nonatomic, retain) NSTimer *timer1;
@property (nonatomic, retain) IBOutlet UILabel *lbTimer;
@property (nonatomic, retain) IBOutlet UILabel *lbThread;

@end
