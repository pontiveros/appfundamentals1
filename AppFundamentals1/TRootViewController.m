//
//  TRootViewController.m
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/17/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import "TRootViewController.h"
#import "TListViewController.h"
#import "TMultiThreadViewController.h"
#import "TWebViewController.h"
#import "TGesturesViewController.h"
#import "TCommunicationViewController.h"


@interface TRootViewController ()

@end

@implementation TRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Home";
        items = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // dummy
    
    [items setObject:@"openCoreDataSample" forKey:@"Core Data sample"];
    [items setObject:@"openMultiThreading" forKey:@"Multi Thread sample"];
    [items setObject:@"openWebView" forKey:@"Web View sample"];
    [items setObject:@"openGesturesView" forKey:@"Gesture recognizer"];
    [items setObject:@"openCommunicationView" forKey:@"Communication"];
}

- (void)dealloc
{
    [items release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openCoreDataSample
{
    TListViewController * listView = [[[TListViewController alloc] init] autorelease];
    [self.navigationController pushViewController:listView animated:YES];
}

- (void)openMultiThreading
{
    TMultiThreadViewController *multiView = [[[TMultiThreadViewController alloc] init] autorelease];
    [self.navigationController pushViewController:multiView animated:YES];
}

- (void)openWebView
{
    TWebViewController *webViewController = [[[TWebViewController alloc] init] autorelease];
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void)openGesturesView
{
    TGesturesViewController *gestureView = [[[TGesturesViewController alloc] init] autorelease];
    [self.navigationController pushViewController:gestureView animated:YES];
}

- (void)openCommunicationView
{
    TCommunicationViewController *commView = [[[TCommunicationViewController alloc] init] autorelease];
    [self.navigationController pushViewController:commView animated:YES];
}

#pragma mark - UITableViewDelegate 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
        NSString *stringSelector = [items objectForKey:[[items allKeys] objectAtIndex:indexPath.row]];
        SEL         signatureSel = NSSelectorFromString(stringSelector);
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[self methodSignatureForSelector:signatureSel]];
        
        [invocation setTarget:self];
        [invocation setSelector:signatureSel];
        [invocation invoke];
        
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
    } @catch (NSException *err) {
        NSLog(@"An error has occurred :%@", [err description]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:[err description] delegate:nil cancelButtonTitle:@"Accept" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [[[UITableViewCell alloc] init] autorelease];
    NSString *key = [[items allKeys] objectAtIndex:indexPath.row];
    [cell.textLabel setText:key];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [items count];
}

@end
