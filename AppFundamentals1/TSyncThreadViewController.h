//
//  TSyncThreadViewController.h
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/18/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSyncThreadViewController : UIViewController
{
    IBOutlet UITextField *edValueA;
    IBOutlet UITextField *edValueB;
    IBOutlet UITextField *edResult;
    IBOutlet UILabel     *progress;
    
    float numberA;
    float numberB;
    float result;
}

@property (nonatomic, retain) NSThread *worker1;

@end
