//
//  Customer.m
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/23/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import "Customer.h"


@implementation Customer

@dynamic code;
@dynamic fullname;
@dynamic phone;
@dynamic county;

@end
