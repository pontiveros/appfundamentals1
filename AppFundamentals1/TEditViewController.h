//
//  TEditViewController.h
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/23/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TEditViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>
{
    IBOutlet UITextField  *edCode;
    IBOutlet UITextField  *edFullname;
    IBOutlet UITextField  *edPhone;
    IBOutlet UIPickerView *cbCounty;
    
    NSMutableArray *locations;
    NSString       *locationSelected;
}

@end
