//
//  TRootViewController.h
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/17/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRootViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableDictionary * items;
}

@end
