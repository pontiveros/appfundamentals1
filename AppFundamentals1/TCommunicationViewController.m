//
//  TCommunicationViewController.m
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/24/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import "TCommunicationViewController.h"
#import "TAppDelegate.h"
#import "TLocation.h"


@interface TCommunicationViewController ()

@end

@implementation TCommunicationViewController

@synthesize logView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"communication";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)updateLogView:(NSString*)value
{
    NSString *currentLog = self.logView.text;
    
    if ([currentLog length] > 0) {
        [self.logView setText:[NSString stringWithFormat:@"%@\n%@", currentLog, value]];
    } else {
        [self.logView setText:value];
    }
}

- (IBAction)onTouchSync:(id)sender
{
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue setMaxConcurrentOperationCount:4];

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost/webmobile/locations.php"]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if (!error) {
                                   NSError *jsonError = nil;
                                   
                                   // NSString *input1 = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   // NSData   *input2 = [input1 dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                                   // NSString *input3 = [[NSString alloc] initWithData:input2 encoding:NSUTF8StringEncoding];
                                   // NSString *other  = [[NSString alloc] initWithData:input2 encoding:NSASCIIStringEncoding];
                                   
                                   NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                                                                 options:NSJSONReadingMutableContainers
                                                                                                   error:&jsonError];
                                   
                                   if (jsonError) {
                                       NSLog(@"%@", [jsonError description]);
                                   } else {
                                       
                                       NSMutableString *result = [[NSMutableString alloc] init];
                                       TAppDelegate *appDelegate = (TAppDelegate*)[UIApplication sharedApplication].delegate;
                                       
                                       for (NSDictionary *dict in jsonArray) {
                                           NSLog(@"%@", [dict objectForKey:@"instance"]);
                                           NSLog(@"%@", [dict objectForKey:@"location_name"]);
                                           [result appendString:[dict objectForKey:@"location_name"]];
                                           NSString *s1 = [dict objectForKey:@"location_name"];
                                           
                                           TLocation *addLocation = [NSEntityDescription insertNewObjectForEntityForName:@"TLocation"
                                                                                                     inManagedObjectContext:appDelegate.managedObjectContext];
                                           NSError *errNewRow = nil;
                                           
                                           NSNumber *codeNum    = [NSNumber numberWithChar:[((NSString*)[dict objectForKey:@"instance"]) intValue]];
                                           NSString *locName    = [NSString stringWithString:[dict objectForKey:@"location_name"]];
                                           NSNumber *status     = [NSNumber numberWithChar:[((NSString*)[dict objectForKey:@"status"]) intValue]];
                                           
                                           addLocation.code     = codeNum;
                                           addLocation.fullname = locName;
                                           addLocation.status   = status;
                                           
                                           if (![appDelegate.managedObjectContext save:&errNewRow]){
                                               NSLog(@"Error trying to save locations : %@", [errNewRow description]);
                                           } else {
                                               NSLog(@"Save location %@ OK. ", locName);
                                           }
                                           
                                           [self performSelectorOnMainThread:@selector(updateLogView:) withObject:s1 waitUntilDone:NO];
                                       }
                                       
                                       NSLog(@"Here...");
                                   }
                                   
                               } else {
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                                                   message:@"Trying to communicate."
                                                                                  delegate:nil
                                                                         cancelButtonTitle:@"Accept"
                                                                         otherButtonTitles:nil];
                                   [alert show];
                                   [alert release];
                               }
                           }
    ];
}


@end
