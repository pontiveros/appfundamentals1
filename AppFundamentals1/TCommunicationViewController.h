//
//  TCommunicationViewController.h
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/24/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCommunicationViewController : UIViewController
{
}

@property (nonatomic, retain)IBOutlet UITextView *logView;

@end
