//
//  TLocation.h
//  AppFundamentals1
//
//  Created by Pedro Ontiveros on 10/25/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TLocation : NSManagedObject

@property (nonatomic, retain) NSNumber * code;
@property (nonatomic, retain) NSString * fullname;
@property (nonatomic, retain) NSNumber * status;

@end
