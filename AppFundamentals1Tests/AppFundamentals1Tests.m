//
//  AppFundamentals1Tests.m
//  AppFundamentals1Tests
//
//  Created by Pedro Ontiveros on 10/17/13.
//  Copyright (c) 2013 qbxsoft. All rights reserved.
//

#import "AppFundamentals1Tests.h"

@implementation AppFundamentals1Tests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in AppFundamentals1Tests");
}

@end
